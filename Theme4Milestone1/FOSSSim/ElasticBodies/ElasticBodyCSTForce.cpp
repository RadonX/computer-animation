#include "ElasticBodyCSTForce.h"
#include <assert.h>

ElasticBodyCSTForce::ElasticBodyCSTForce( int idx1, int idx2, int idx3, const scalar& youngsmodulus, const scalar& poissonratio, const Vector2s& xb1, const Vector2s& xb2, const Vector2s& xb3 )
: Force()
, m_idx1(idx1)
, m_idx2(idx2)
, m_idx3(idx3)
, m_youngs_modulus(youngsmodulus)
, m_poisson_ratio(poissonratio)
, m_xb1(xb1)
, m_xb2(xb2)
, m_xb3(xb3)
{
    assert( idx1 >= 0 );
    assert( idx2 >= 0 );
    assert( idx3 >= 0 );
    assert( idx1 != idx2 );
    assert( idx1 != idx3 );
    assert( idx2 != idx3 );
    assert( youngsmodulus >= 0.0 );
    assert( poissonratio >= 0.0 );
}

ElasticBodyCSTForce::~ElasticBodyCSTForce()
{}

void ElasticBodyCSTForce::addEnergyToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, scalar& E )
{
    assert( x.size() == v.size() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );
    assert( m_idx3 >= 0 );  assert( m_idx3 < x.size()/2 );

}

void ElasticBodyCSTForce::addGradEToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, VectorXs& gradE )
{
    assert( x.size() == v.size() );
    assert( x.size() == gradE.size() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );
    assert( m_idx3 >= 0 );  assert( m_idx3 < x.size()/2 );
        
}

void ElasticBodyCSTForce::addHessXToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, MatrixXs& hessE )
{
    assert( x.size() == v.size() );
    assert( x.size() == m.size() );
    assert( x.size() == hessE.rows() );
    assert( x.size() == hessE.cols() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );
    assert( m_idx3 >= 0 );  assert( m_idx3 < x.size()/2 );
  
    
}

void ElasticBodyCSTForce::addHessVToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, MatrixXs& hessE )
{
    assert( x.size() == v.size() );
    assert( x.size() == m.size() );
    assert( x.size() == hessE.rows() );
    assert( x.size() == hessE.cols() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );
    assert( m_idx3 >= 0 );  assert( m_idx3 < x.size()/2 );
    
    
}

Force* ElasticBodyCSTForce::createNewCopy()
{
    return new ElasticBodyCSTForce(*this);
}
