#include "ElasticBodySpringForce.h"
#include <assert.h>

ElasticBodySpringForce::ElasticBodySpringForce( int idx1, int idx2, const scalar& alpha, const scalar& l0 )
: Force()
, m_idx1(idx1)
, m_idx2(idx2)
, m_alpha(alpha)
, m_l0(l0)
{
    assert( idx1 >= 0 );
    assert( idx2 >= 0 );
    assert( idx1 != idx2 );
    assert( m_alpha >= 0.0 );
    assert( m_l0 >= 0.0 );
}

ElasticBodySpringForce::~ElasticBodySpringForce()
{}

void ElasticBodySpringForce::addEnergyToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, scalar& E )
{
    assert( x.size() == v.size() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );

}

void ElasticBodySpringForce::addGradEToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, VectorXs& gradE )
{
    assert( x.size() == v.size() );
    assert( x.size() == gradE.size() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );

}

void ElasticBodySpringForce::addHessXToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, MatrixXs& hessE )
{
    assert( x.size() == v.size() );
    assert( x.size() == m.size() );
    assert( x.size() == hessE.rows() );
    assert( x.size() == hessE.cols() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );
    
}

void ElasticBodySpringForce::addHessVToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, MatrixXs& hessE )
{
    assert( x.size() == v.size() );
    assert( x.size() == m.size() );
    assert( x.size() == hessE.rows() );
    assert( x.size() == hessE.cols() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );
    
}

Force* ElasticBodySpringForce::createNewCopy()
{
    return new ElasticBodySpringForce(*this);
}
