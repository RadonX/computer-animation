#include "ElasticBodyBendingForce.h"
#include <assert.h>

ElasticBodyBendingForce::ElasticBodyBendingForce( int idx1, int idx2, int idx3, const scalar& alpha, const scalar& theta0, const scalar& eb1n, const scalar& eb2n )
: Force()
, m_idx1(idx1)
, m_idx2(idx2)
, m_idx3(idx3)
, m_alpha(alpha)
, m_theta0(theta0)
, m_eb1n(eb1n)
, m_eb2n(eb2n)
{
    assert( idx1 >= 0 );
    assert( idx2 >= 0 );
    assert( idx3 >= 0 );
    assert( idx1 != idx2 );
    assert( idx1 != idx3 );
    assert( idx2 != idx3 );
    assert( m_alpha >= 0.0 );
    assert( m_eb1n >= 0.0 );
    assert( m_eb2n >= 0.0 );
}

ElasticBodyBendingForce::~ElasticBodyBendingForce()
{}

void ElasticBodyBendingForce::addEnergyToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, scalar& E )
{
    assert( x.size() == v.size() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );
    assert( m_idx3 >= 0 );  assert( m_idx3 < x.size()/2 );
        
}

void ElasticBodyBendingForce::addGradEToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, VectorXs& gradE )
{
    assert( x.size() == v.size() );
    assert( x.size() == gradE.size() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );
    assert( m_idx3 >= 0 );  assert( m_idx3 < x.size()/2 );
    
    
}

void ElasticBodyBendingForce::addHessXToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, MatrixXs& hessE )
{
    assert( x.size() == v.size() );
    assert( x.size() == m.size() );
    assert( x.size() == hessE.rows() );
    assert( x.size() == hessE.cols() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );
    assert( m_idx3 >= 0 );  assert( m_idx3 < x.size()/2 );
  
    
}

void ElasticBodyBendingForce::addHessVToTotal( const VectorXs& x, const VectorXs& v, const VectorXs& m, MatrixXs& hessE )
{
    assert( x.size() == v.size() );
    assert( x.size() == m.size() );
    assert( x.size() == hessE.rows() );
    assert( x.size() == hessE.cols() );
    assert( x.size()%2 == 0 );
    assert( m_idx1 >= 0 );  assert( m_idx1 < x.size()/2 );
    assert( m_idx2 >= 0 );  assert( m_idx2 < x.size()/2 );
    assert( m_idx3 >= 0 );  assert( m_idx3 < x.size()/2 );
    

}

Force* ElasticBodyBendingForce::createNewCopy()
{
    return new ElasticBodyBendingForce(*this);
}
