#ifndef MATHEMATICA_FUNCS_H__
#define MATHEMATICA_FUNCS_H__

#include <math.h>

namespace MathematicaFuncs
{
    inline double Sqrt(double x) { assert(x>=0); return sqrt(x); }
    inline double Abs(double x) { return fabs(x); }
    inline double Power(double x, double e) { assert(e==2); return x*x; }
    inline double ArcTan(double x, double y) { return atan2(y, x); }
    
}

#endif
