#ifndef __RIGID_BODY_FORWARD_BACKWARD_EULER_H__
#define __RIGID_BODY_FORWARD_BACKWARD_EULER_H__

#include "RigidBodyStepper.h"

class RigidBodyForwardBackwardEuler : public RigidBodyStepper
{
public:

  virtual ~RigidBodyForwardBackwardEuler();

  virtual bool stepScene( RigidBodyScene& scene, scalar dt );

  virtual std::string getName() const;

};

#endif
