#include "RigidBodySpringForce.h"

RigidBodySpringForce::RigidBodySpringForce( const scalar& k, const scalar& l0, int rb0, const Vector2s& vc0, int rb1, const Vector2s& vc1 )
: m_k(k)
, m_l0(l0)
, m_rb0(rb0)
, m_rb1(rb1)
, m_vc0(vc0)
, m_vc1(vc1)
{
  assert( m_k >= 0.0 );
  assert( m_l0 >= 0.0 );
  assert( m_rb0 >= -1 );
  assert( m_rb1 >= -1 );
}

RigidBodySpringForce::~RigidBodySpringForce()
{
}

bool RigidBodySpringForce::isConservative() const
{
  return true;
}

int RigidBodySpringForce::getFirstRigidBody() const
{
  return m_rb0;
}

int RigidBodySpringForce::getSecondRigidBody() const
{
  return m_rb1;
}

const Vector2s& RigidBodySpringForce::getFirstEndpoint() const
{
  return m_vc0;
}

const Vector2s& RigidBodySpringForce::getSecondEndpoint() const
{
  return m_vc1;
}


inline bool RigidBodySpringForce::firstEndpointIsFixed() const
{
  return m_rb0 == -1;
}

inline bool RigidBodySpringForce::secondEndpointIsFixed() const
{
  return m_rb1 == -1;
}

Vector2s RigidBodySpringForce::computeFirstEndpoint( const std::vector<RigidBody>& rbs ) const
{
  assert( m_rb0 >= -1 ); assert( m_rb0 < (int) rbs.size() );
  // If the endpoint is fixed, we store its world space position. Otherwise we must compute the world space position.
  return firstEndpointIsFixed() ? m_vc0 : rbs[m_rb0].computeWorldSpacePosition(m_vc0);
}

Vector2s RigidBodySpringForce::computeSecondEndpoint( const std::vector<RigidBody>& rbs ) const
{
  assert( m_rb1 >= -1 ); assert( m_rb1 < (int) rbs.size() );
  // If the endpoint is fixed, we store its world space position. Otherwise we must compute the world space position.
  return secondEndpointIsFixed() ? m_vc1 : rbs[m_rb1].computeWorldSpacePosition(m_vc1);
}

scalar RigidBodySpringForce::computePotentialEnergy( const std::vector<RigidBody>& rbs )
{
  assert( m_rb0 >= -1 ); assert( m_rb0 < (int) rbs.size() );
  assert( m_rb1 >= -1 ); assert( m_rb1 < (int) rbs.size() );

  // Add Theme 3 Milestone 1 code here.
  scalar l = (computeFirstEndpoint(rbs) - computeSecondEndpoint(rbs)).norm();
  return 0.5 * m_k * (l - m_l0) * (l - m_l0);
}

void RigidBodySpringForce::computeForceAndTorque( std::vector<RigidBody>& rbs )
{
  assert( m_rb0 >= -1 ); assert( m_rb0 < (int) rbs.size() );
  assert( m_rb1 >= -1 ); assert( m_rb1 < (int) rbs.size() );

  // Add Theme 3 Milestone 1 code here.
  if (firstEndpointIsFixed() && secondEndpointIsFixed()) return;

  Vector2s nhat = computeFirstEndpoint(rbs) - computeSecondEndpoint(rbs);
  // x.segment<2>(2*m_endpoints.second)-x.segment<2>(2*m_endpoints.first);
  scalar l = nhat.norm();
  assert( l != 0.0 ); // or simply return??
  nhat /= l;
  Vector2s f0 = - m_k * (l - m_l0) * nhat;
  if (!firstEndpointIsFixed()) {
    rbs[m_rb0].getForce() += f0;
    rbs[m_rb0].getTorque() += mathutils::crossTwoD(m_vc0, f0);
  }
  if (!secondEndpointIsFixed()) {
    rbs[m_rb1].getForce() -= f0;
    rbs[m_rb1].getTorque() -= mathutils::crossTwoD(m_vc1, f0);
  }
}

RigidBodyForce* RigidBodySpringForce::createNewCopy()
{
  return new RigidBodySpringForce( m_k, m_l0, m_rb0, m_vc0, m_rb1, m_vc1 );
}
