#include "RigidBodyExplicitEuler.h"

RigidBodyExplicitEuler::~RigidBodyExplicitEuler()
{
}

bool RigidBodyExplicitEuler::stepScene( RigidBodyScene& scene, scalar dt )
{
  // Clear any previously computed forces in the rigid bodies
  std::vector<RigidBody>& rbs = scene.getRigidBodies();
  for( std::vector<RigidBody>::size_type i = 0; i < rbs.size(); ++i ) rbs[i].getForce().setZero();
  for( std::vector<RigidBody>::size_type i = 0; i < rbs.size(); ++i ) rbs[i].getTorque() = 0.0;

  // Add each force's contribution to each rigid body using previous step's state
  std::vector<RigidBodyForce*>& frcs = scene.getForces();
  for( std::vector<RigidBodyForce*>::size_type i = 0; i < frcs.size(); ++i )
    frcs[i]->computeForceAndTorque(rbs);

  // For each rigid body
  for( std::vector<RigidBody>::size_type i = 0; i < rbs.size(); ++i )
  {
    // Add Theme 3 Milestone 1 code here.
    RigidBody &rigidBody = rbs[i];
    Vector2s &V = rigidBody.getV();
    scalar &omega = rigidBody.getOmega();

    // Step positions forward based on start-of-step velocity
    rigidBody.getX() += dt * V;
    rigidBody.getTheta() += dt * omega; // > 2*PI ??
    // Step velocities forward based on start-of-step forces
    V += dt / rigidBody.getM() * rigidBody.getForce();
    omega += dt * rigidBody.getTorque() / rigidBody.getI();
  }

  for( std::vector<RigidBodyForce*>::size_type i = 0; i < rbs.size(); ++i ) rbs[i].updateDerivedQuantities();

  return true;
}

std::string RigidBodyExplicitEuler::getName() const
{
  return "Rigid Body Explicit Euler";
}
