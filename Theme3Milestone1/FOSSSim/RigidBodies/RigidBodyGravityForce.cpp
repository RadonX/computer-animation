#include "RigidBodyGravityForce.h"

RigidBodyGravityForce::RigidBodyGravityForce( const Vector2s& g )
: m_g(g)
{}

RigidBodyGravityForce::~RigidBodyGravityForce()
{}

bool RigidBodyGravityForce::isConservative() const
{
  return true;
}

scalar RigidBodyGravityForce::computePotentialEnergy( const std::vector<RigidBody>& rbs )
{
    // Add Theme 3 Milestone 1 code here.
    // - M g.dot(X)
    scalar totalPE = 0;
    for( std::vector<RigidBody>::size_type i = 0; i < rbs.size(); ++i ) {
        totalPE -= rbs[i].getM() * m_g.dot(rbs[i].getX());
    }
    return totalPE;
}

void RigidBodyGravityForce::computeForceAndTorque( std::vector<RigidBody>& rbs )
{
    // Add Theme 3 Milestone 1 code here.

    for( std::vector<RigidBody>::size_type i = 0; i < rbs.size(); ++i ) {
        rbs[i].getForce() += rbs[i].getM() * m_g; // fixed rigid body ??
        // Torque: Ldot_spin = 0
    }
}

RigidBodyForce* RigidBodyGravityForce::createNewCopy()
{
  return new RigidBodyGravityForce(m_g);
}
