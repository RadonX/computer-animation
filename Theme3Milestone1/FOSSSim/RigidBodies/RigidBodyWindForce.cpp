#include "RigidBodyWindForce.h"


RigidBodyWindForce::RigidBodyWindForce( int num_quad_points, const scalar& beta, const Vector2s& wind )
: m_num_quadrature_points(num_quad_points)
, m_beta(beta)
, m_wind(wind)
{
  assert( m_num_quadrature_points >= 2 );
  assert( beta >= 0.0 );
}

RigidBodyWindForce::~RigidBodyWindForce()
{
}

bool RigidBodyWindForce::isConservative() const
{
  return false;
}

scalar RigidBodyWindForce::computePotentialEnergy( const std::vector<RigidBody>& rbs )
{
  // TODO: Clean up this warning
  std::cout << "WARNING: TRYING TO COMPUTE POTENTIAL ENERGY FOR A NON-CONSERVATIVE FORCE." << std::endl;

  return 0.0;
}

void RigidBodyWindForce::computeForceAndTorque( std::vector<RigidBody>& rbs )
{
    // Add Theme 3 Milestone 1 code here.
    for (int rbid = 0; rbid < rbs.size(); rbid++) {
        Vector2s force(0,0);
        scalar torque = 0;
        RigidBody &rigidBody = rbs[rbid];
        int N = rigidBody.getNumEdges();

        for (int i = 0; i < N; ++i) {
            Vector2s firstEndPoint = rigidBody.getWorldSpaceVertex(i);
            Vector2s secondEndPoint = rigidBody.getWorldSpaceVertex((i + 1) % N);
            //computeWorldSpaceEdge
            Vector2s xx = (secondEndPoint - firstEndPoint) / (2*m_num_quadrature_points);
            Vector2s nhat = mathutils::rotateCounterClockwise90degrees(secondEndPoint - firstEndPoint);
            scalar l = nhat.norm();
            nhat /= l;
            scalar forceScale = 0;
            for (int j = 0; j < m_num_quadrature_points; ++j) {
                Vector2s xj = firstEndPoint + (2*j + 1) * xx;
                Vector2s rj = xj - rigidBody.getX();
                Vector2s vj = rigidBody.computeWorldSpaceVelocity(xj);
                forceScale += nhat.dot(m_wind - vj);
                torque += mathutils::crossTwoD(rj, nhat) *
                        l * m_beta / m_num_quadrature_points * nhat.dot(m_wind - vj);
            }
            force += l * m_beta / m_num_quadrature_points * forceScale * nhat;
        }
        rigidBody.getForce() += force;
        rigidBody.getTorque() += torque;
    }
// fixed ??

}

RigidBodyForce* RigidBodyWindForce::createNewCopy()
{
  return new RigidBodyWindForce(m_num_quadrature_points,m_beta,m_wind);
}
