#include "RigidBodyLCPCollisionResolver.h"

void RigidBodyLCPCollisionResolver::resolveCollisions( std::vector<RigidBody>& rbs, const std::set<RigidBodyCollision>& rbcs )
{
  // Add Theme 3 Milestone 2 code here.
  if (rbcs.size() == 0) return;
  int num = rbs.size();

  // init M, qdot
  VectorXs Mdiagonal(3*num);
  VectorXs qdot(3*num);
  for (int i = 0; i < num; i++) {
    RigidBody &rigidBody = rbs[i];
    Mdiagonal.segment<3>(3*i) << rigidBody.getM(), rigidBody.getM(), rigidBody.getI();
    qdot.segment<2>(3*i) = rigidBody.getV();
    qdot(3*i+2) = rigidBody.getOmega();
  }
  MatrixXs Minv = Mdiagonal.cwiseInverse().asDiagonal();

  // Fixed body
  for (int i = 0; i < num; i++) {
    if (rbs[i].isFixed()) { // or remove relevant rows/cols in N, qdot, M
      Minv(3*i, 3*i) = 0; Minv(3*i+1, 3*i+1) = 0; Minv(3*i+2, 3*i+2) = 0;
    }
  }

  // calculate N
  MatrixXs N = MatrixXs::Zero(3*num, rbcs.size());
  int j = 0;
  MatrixXs gamma(2,6);
  Matrix2s RR; RR << 0, -1, 1, 0; // hope I did the right math
  for (std::set<RigidBodyCollision>::iterator it = rbcs.begin(); it != rbcs.end(); ++it) {
    Vector2s temp1 = RR * (it->r0);
    Vector2s temp2 = -(RR * (it->r1));
    gamma << 1, 0, temp1(0), -1, 0, temp2(0),
             0, 1, temp1(1), 0, -1, temp2(1);
    VectorXs eta = gamma.transpose() * (it->nhat); // size: 6*1
    N.block<3, 1>(3*(it->i0), j) = eta.segment<3>(0);
    N.block<3, 1>(3*(it->i1), j) = eta.segment<3>(3);
    ++j;
  }

  VectorXs b =  N.transpose() * qdot;
  MatrixXs A = N.transpose() * (Minv * N);

  std::cout << A << std::endl;
  std::cout << b << std::endl;

  VectorXs lambda(rbcs.size());
  lcputils::solveLCPwithODE(A, b, lambda);

  VectorXs qdotnew = qdot + (Minv * N) * lambda;
  for (int i = 0; i < num; i++) {
    RigidBody &rigidBody = rbs[i];
    rigidBody.getV() = qdotnew.segment<2>(3*i);
    rigidBody.getOmega() = qdotnew(3*i+2);
  }

}

std::string RigidBodyLCPCollisionResolver::getName() const
{
  return "lcp";
}
