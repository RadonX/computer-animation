#include "RigidBodyVelocityProjectionCollisionResolver.h"

inline void setMatN(MatrixXs &N, const std::vector<int> &idx, const Matrix2s &RR,
    int rbi, const Vector2s& r, const Vector2s &nhat, int col) {
  if (idx[rbi] == -1) return; // skip fixed body

  Eigen::Matrix<scalar, 2, 3> gamma;
  Vector2s temp = RR * r;
  gamma << 1, 0, temp(0),
           0, 1, temp(1);
  VectorXs eta = gamma.transpose() * nhat; // size: 3*1
  N.block<3, 1>(3*idx[rbi], col) = eta;
}

void RigidBodyVelocityProjectionCollisionResolver::resolveCollisions( std::vector<RigidBody>& rbs, const std::set<RigidBodyCollision>& rbcs )
{
  // Add Theme 3 Milestone 2 code here.
  // For detailed documentation of QuadProg++, please see FOSSSim/quadprog/QuadProg++.hh

  if (rbcs.size() == 0) return;
  int num = rbs.size();

  // Index of rigidBodies in N, M, qdot
  // Used in shrinking M, qdot and N
  std::vector<int> idx(num);
  int m = 0;
  for (int i = 0; i < num; i++) {
    idx[i] = (rbs[i].isFixed())? -1: (m++);
  }

  // init M, qdot
  // G: Matrix in quadratic form of objective, 1/2*v^T*M*v
  QuadProgPP::Matrix<scalar> G;
  G.resize(3*m,3*m);
  for (int i = 0; i < 3*m; i++)
    for (int j = 0; j < 3*m; j++)
      G[i][j] = 0;
  MatrixXs M = MatrixXs::Zero(3*m, 3*m);
  VectorXs qdot(3*m);
  for (int j = 0; j < num; j++) {
    RigidBody &rigidBody = rbs[j];
    if (rigidBody.isFixed()) continue;
    int i = idx[j];
    G[3*i][3*i] = rigidBody.getM();
    G[3*i+1][3*i+1] = rigidBody.getM();
    G[3*i+2][3*i+2] = rigidBody.getI();
    M(3*i,3*i) = rigidBody.getM();
    M(3*i+1,3*i+1) = rigidBody.getM();
    M(3*i+2,3*i+2) = rigidBody.getI();
    qdot.segment<2>(3*i) = rigidBody.getV();
    qdot(3*i+2) = rigidBody.getOmega();
  }

  // g0: -v^T*M*qdot
  QuadProgPP::Vector<scalar> g0;
  VectorXs g0tmp = - (M * qdot);
  g0.resize(3*m);
  for (int i = 0; i < 3*m; i++) g0[i] = g0tmp(i);

  // No Equality constraints
  QuadProgPP::Matrix<scalar> CE; CE.resize(3*m, 0);
  QuadProgPP::Vector<scalar> ce0; ce0.resize(0);

  // calculate N, copied from "RigidBodyLCPCollisionResolver.cpp"
  MatrixXs N = MatrixXs::Zero(3*m, rbcs.size());
  static Matrix2s RR; RR << 0, -1, 1, 0; // hope I did the right math
  int j = 0;
  for (std::set<RigidBodyCollision>::iterator it = rbcs.begin(); it != rbcs.end(); ++it) {
    setMatN(N, idx, RR, it->i0, it->r0, it->nhat, j);
    setMatN(N, idx, RR, it->i1, it->r1, -(it->nhat), j);
    ++j;
  }

  // Inequality constraints
  QuadProgPP::Matrix<scalar> CI; CI.resize(3*m,rbcs.size());
  QuadProgPP::Vector<scalar> ci0(0.0,rbcs.size()); // Constant term
  for (int i = 0; i < 3*m; i++)
    for (int j = 0; j < rbcs.size(); j++)
      CI[i][j] = N(i,j);

  std::cout << M << std::endl;
  std::cout << g0tmp << std::endl;
  std::cout << N << std::endl;

  // Solution
  QuadProgPP::Vector<scalar> v; v.resize(3*m);
  solve_quadprog(G, g0, CE, ce0, CI, ci0, v);
  for (int j = 0; j < num; j++) {
    RigidBody &rigidBody = rbs[j];
    if (rigidBody.isFixed()) continue;
    int i = idx[j];
    rigidBody.getV() << v[3*i], v[3*i+1];
    rigidBody.getOmega() = v[3*i+2];
  }

}

std::string RigidBodyVelocityProjectionCollisionResolver::getName() const
{
  return "velocity-projection";
}
