#include "RigidBodyAllPairsCollisionDetector.h"

inline scalar SimpleDistanceParticleEdge(const Vector2s &x1,
        const Vector2s &x2, const Vector2s &x3, Vector2s &n, Vector2s &closest)
{
  assert(x2 != x3);
  double alpha = (x1-x2).dot(x3-x2)/(x3-x2).dot(x3-x2);
  alpha = std::min(1.0, std::max(0.0, alpha));
  closest = x2 + alpha*(x3-x2);
  n = closest - x1; // n = x1 - closest;
  return n.norm();
}

void RigidBodyAllPairsCollisionDetector::detectCollisions( const std::vector<RigidBody>& rbs, std::set<RigidBodyCollision>& collisions )
{
    // Add Theme 3 Milestone 2 code here.
    // Compute all vertex-edge collisions between all pairs of rigid bodies.
    for (std::vector<RigidBody>::size_type rbi = 0; rbi < rbs.size(); ++rbi) {
        const RigidBody &rigidBody = rbs[rbi];
        for (int i = 0; i < rigidBody.getNumEdges(); ++i) {
            Vector2s x2 = rigidBody.getWorldSpaceVertex(i);
            Vector2s x3 = rigidBody.getWorldSpaceVertex((i+1)%rigidBody.getNumVertices());
            for (std::vector<RigidBody>::size_type rbj = 0; rbj < rbs.size(); ++rbj) {
                if (rbi == rbj) continue;
                const RigidBody &otherRigidBody = rbs[rbj];
                for (int j = 0; j < otherRigidBody.getNumVertices(); j++) {
                    Vector2s x1 = otherRigidBody.getWorldSpaceVertex(j);
                    Vector2s n, closest;
                    scalar l = SimpleDistanceParticleEdge(x1, x2, x3, n, closest);
                    if (l < otherRigidBody.getRadius() + rigidBody.getRadius() ) {
                        // collision occurs
                        collisions.insert(RigidBodyCollision(rbi, rbj,
                                closest - rigidBody.getX(), x1 - otherRigidBody.getX(), n/l));
                                // r0 & r1: world space vector; n: i1 -> i0
                    }
                }
            }
        }
    }

}

std::string RigidBodyAllPairsCollisionDetector::getName() const
{
  return "all-pairs";
}
