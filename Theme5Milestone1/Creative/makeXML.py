#!/usr/bin/env python3

out = open('rx2135.xml', 'w')
out.write("""<scene>
  <simtype type="joint-motor-body"/>
  <description text=""/>
  <maxsimfreq max="100.0"/>
  <duration time="120.0"/>

  <rigidbodyintegrator type="symplectic-euler" dt="0.01"/>
  <rigidbodycollisionhandling detection="ground"/>
  <rigidbodygravityforce fx="0" fy="-100"/>

  <!-- detectors -->
  <detector type="mr-min-jump-height" jointmotorbody="0" ymin="300" ymax="600" njump="5"/>
""")
#   <viewport cx="1500" cy="0" size="1500"/>

count = 0


# a slice of snow
def reflection(x, a):
    return a - (x - a)
px = [265,307,304,368,367,374,305,303,345,353,302,302,328,333,301,298,363,348,373,380,403,380,405,420,456,424,459,476,503,528,498,537,513,475,439,470,452,422,396,415,395,378,310]
py = [154,154,193,181,180,202,209,255,244,268,268,298,293,312,313,394,354,328,314,342,328,285,273,316,297,239,224,282,266,305,320,374,390,331,348,383,399,358,370,398,407,381,415]
mid = ( (265+307)/2.0, 415.0 )
size = len(px)
for i in range(size - 1, -1, -1):
    px.append(px[i])
    py.append(reflection(py[i], mid[1]))
for i in range(0, size):
    px.append(reflection(px[i], mid[0]))
    py.append(reflection(py[i], mid[1]))
for i in range(size - 1, -1, -1):
    px.append(reflection(px[i], mid[0]))
    py.append(py[i])

scale = 1.50

snowrad = 500/scale
snowspace = []
def noCollision(x, y):
    if (y > 2*x - 3100 and y > - 2*x + 2000): return False #
    for coord in snowspace:
        if (x > coord[0] - snowrad and x < coord[0] + snowrad and y > coord[1] - snowrad and y < coord[1] + snowrad): return False
    snowspace.append((x,y))
    return True

import random
for snowi in range(0,2):
    y = 1500 / scale
    x = random.randint(0,6*y)
    # while not noCollision(x, y):
    #     x = random.randint(0,2000)
    #     y = random.randint(0,2000)
    omega = random.random()
    # omega = 0
    center = (x + mid[0]/scale, y - mid[1]/scale)
    for i in range(0, len(px)):
        out.write('<rigidbodyvertex x="'+str(x+px[i]/scale)+'" y="'+str(y-py[i]/scale)+'" m="1"/>')

    out.write('<rigidbody ')
    for i in range(0, len(px)):
        out.write('p="'+str(count)+'" ')
        count += 1
    out.write(' vx="0.0" vy="0.0" omega="'+str(omega)+'" r="8"/>\n')
    out.write('<rigidbodycolor body="'+str(2*snowi)+'" r="1" g="1" b="1"/>\n\n')

    # foot
    out.write('<rigidbodyvertex x="'+str(center[0])+'" y="0" m="0.1"/>')
    out.write('<rigidbody p="'+str(count)+'" vx="0.0" vy="0.0" omega="0.0" r="10"/>\n')
    out.write('<rigidbodycolor body="'+str(2*snowi+1)+'" r="'+str(75/255)+'" g="'+str(129/255)+'" b="'+str(229/255)+'"/>\n\n')
    count += 1

    out.write("""
    <jointmotorbody type="monopod-robot">
      <legspring k="1000" l0="500" l0min="300" l0max="600" b="2.0"/>
    """)
    out.write('<link body="'+str(2*snowi)+'" parent="-1" axisx="0" axisy="0"/>\n')
    out.write('<link body="'+str(2*snowi+1)+'" parent="'+str(2*snowi)+'"  axisx="'+str(center[0])+'" axisy="'+str(center[1])+'"/>\n</jointmotorbody>\n')


out.write('<backgroundcolor r="'+str(75/255)+'" g="'+str(129/255)+'" b="'+str(229/255)+'"/>\n')
out.write("""</scene>""")
out.close()
