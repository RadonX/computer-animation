#ifndef __MONOPOD_ROBOT_CONTROLLER_STRATEGY_H__
#define __MONOPOD_ROBOT_CONTROLLER_STRATEGY_H__

#include "MonopodRobot.h"
#include "JointMotorBodyScene.h"
#include "JointMotorBodySimulationJudge.h"
#include "JointMotorBodySimulation.h"

extern ExecutableSimulation * g_executable_simulation;

template<typename T>
std::vector<const T*> getDetectorsOfType() {
  const JointMotorBodySimulationJudge * judge = (dynamic_cast<JointMotorBodySimulation *>(g_executable_simulation))->judge();
  std::vector<const T*> result;
  for (int i = 0; i < judge->ndetectors(); i++)
  {
    if (const T* d = dynamic_cast<const T*>(judge->detector(i))) {
      result.push_back(d);
    }
  }

  return result;
}


class MonopodRobotControllerStrategy
{
  static const scalar margin = 0.1;
public:
  struct Action
  {
    scalar legSpringRestLength;
    scalar legTorque;
  };

  MonopodRobotControllerStrategy() :
    jumping_detectors(getDetectorsOfType<JumpingDetector>()),
    landing_detectors(getDetectorsOfType<LandingDetector>()),
    balancing_detectors(getDetectorsOfType<BalancingDetector>()),
    velocity_detectors(getDetectorsOfType<VelocityDetector>()),
    treat_detectors(getDetectorsOfType<TreatDetector>())
  {}

  // the detectors to be satisfied
  std::vector<const JumpingDetector*>   jumping_detectors;
  std::vector<const LandingDetector*>   landing_detectors;
  std::vector<const BalancingDetector*> balancing_detectors;
  std::vector<const VelocityDetector*>  velocity_detectors;
  std::vector<const TreatDetector*>     treat_detectors;

  Action generateAction(const JointMotorBodyScene * scene, const MonopodRobot * robot);
};

#endif
