#include "MonopodRobotControllerStrategy.h"
#include "JointMotorBodySimulation.h"
#include <queue>

#define THETA_TAU 1000
#define GAMMA 4

MonopodRobotControllerStrategy::Action MonopodRobotControllerStrategy::generateAction(const JointMotorBodyScene * scene, const MonopodRobot * robot)
{
  // output of the strategy
  Action action;

  static scalar margin = 0.1;
  // head and foot rigid bodies, their positions and velocities
  const RigidBody * head = robot->rigidBody(robot->head());
  const RigidBody * foot = robot->rigidBody(robot->foot());

  Vector2s head_pos = head->getX();
  Vector2s foot_pos = foot->getX();

  Vector2s head_vel = head->getV();
  Vector2s foot_vel = foot->getV();

  Vector2s gravity = scene->gravity();
  scalar dt = scene->dt();
  static scalar T = 0;
  T += dt;

  // leg spring rest length limits
  scalar ls_k = robot->legSpringForce()->k();
  scalar l0 = robot->legSpringRestLength();
  scalar l0min = robot->legSpringRestLengthMin();
  scalar l0max = robot->legSpringRestLengthMax();
  scalar default_leg_length = (l0min + l0max) / 2;

  ////////////////////////////

  // Add Theme 5 Milestone 1 code here.

  scalar m_h = head->getM();
  static std::vector<const JumpingDetector*>::iterator jit = jumping_detectors.begin();
  static std::vector<const BalancingDetector*>::iterator bit = balancing_detectors.begin();
  static std::vector<const VelocityDetector*>::iterator vit = velocity_detectors.begin();
  static std::vector<const LandingDetector*>::iterator lit = landing_detectors.begin();

  // gravity assumption
  scalar gy = -gravity(1);

  // maintain energy using jumping detector
  bool maintainEnergy = (vit != velocity_detectors.end()) || (lit != landing_detectors.end());
  scalar default_apex_height = 5;

  /* Jumping */
  static bool isInStance = false;
  if (jit != jumping_detectors.end() || maintainEnergy) {
    scalar desired_apex_height = default_apex_height;
    if (jit != jumping_detectors.end()) {
      const JumpingDetector* jumping_detector = *jit;
      scalar min_allowed_apex_height = jumping_detector->ymin();
      scalar max_allowed_apex_height = jumping_detector->ymax();
      // tweak
      desired_apex_height = (min_allowed_apex_height + max_allowed_apex_height) / 2;
    }


    // Compute the predicted apex height
    // - assumption: head has most of the weight of the robot
    scalar lc = robot->legSpringLength();
    std::cout << "lc: " << lc << std::endl;
    scalar lhs = 0.5 * m_h * pow(head_vel(1), 2)
        + 0.5 * ls_k * pow(l0 - lc, 2);
    scalar predicted_apex_height = lhs / m_h / gy + head_pos(1);
    std::cout << "y_p: " << predicted_apex_height << ", y_d: " << desired_apex_height << std::endl;

    if (foot_pos(1) - foot->getRadius() <= margin) {
      // Stance phase
      bool isStrected = (lc - l0 > 0);

      // interpolate the leg rest length from its current length and the desired length
#ifndef METHOD_2
    scalar scale = (desired_apex_height - predicted_apex_height) * (l0 - lc);
    if (scale != 0) {
      l0 += 0.15 * scale; // tweak
    }
#else
      scalar tmp = 2.0 * m_h * gy * (desired_apex_height - predicted_apex_height) / ls_k;
      tmp = sqrt( std::max(0.0, tmp) );
      std::cout << isStrected << ' ' << tmp << std::endl;
      l0 += isStrected? ((lc - l0) - tmp): ((lc - l0) + tmp);
      isInStance = true;
#endif

      std::cout << "stance: " << isStrected << ' ' << l0 << std::endl;

    } else {
      // Flight phase
      scalar dl;
#ifndef METHOD_2
      dl = (default_leg_length - l0) * 0.1;
#else
      if (isInStance) {
        scalar halfFlightTime = sqrt(2.0*predicted_apex_height/gy);
        dl = (default_leg_length - l0) / (0.5*halfFlightTime / dt);//!!
      }
      isInStance = false;
#endif
      l0 += dl;
      std::cout << "flight: " << l0 << std::endl;
    }

    // int allowed_number_of_jumps = jumping_detectors[0]->njump();
    if (jit != jumping_detectors.end() && (*jit)->success()) ++jit;
  }


  /* Balancing */

  scalar tau = 0;

  if (true) { // if (bit != balancing_detectors.end()) {
    // Tau can be overwritten by other detectors

    // const BalancingDetector* balancing_detector = *bit;
    Vector2s n = foot_pos - head->computeWorldSpacePosition(robot->foot()->axis_in_parent_body_space);
    // Vector2s n = foot_pos - head_pos;
    scalar sin_theta = -n(0) / n.norm();
    scalar theta = asin(sin_theta);
    // scalar desired_balancing_theta = acos(exp(-v_d/GAMMA)); // == 0
    tau = THETA_TAU * theta; // your torque controller is pretty rad
    std::cout << "tau: " << tau << std::endl;
    // if (balancing_detector->success()) ++bit;

  }


  /* Velocity */

  if (vit != velocity_detectors.end() && T > 5) {
    const VelocityDetector* velocity_detector = *vit;

    if (foot_pos(1) - foot->getRadius() > margin) {
      // Flight phase
      /*
      Suppose your torque controller can set your angle exactly,
      you then want to set it to an angle that will allow you to liftoff
      and reach your goal. So from the goal, back-solve to figure out the
      liftoff angle, and from the liftoff angle and your current velocity,
      back-solve to get what the touchdown angle is.
      */

      scalar min_allowed_horizontal_velocity = velocity_detector->vmin();
      scalar max_allowed_horizontal_velocity = velocity_detector->vmax();
      scalar desired_horizontal_velocity = (min_allowed_horizontal_velocity + max_allowed_horizontal_velocity) / 2;

      // Vector2s n = foot_pos - head->computeWorldSpacePosition(robot->foot()->axis_in_parent_body_space);
      Vector2s n = foot_pos - head_pos;
      n /= n.norm();
      scalar theta_c = asin(-n(0));

      scalar v_d = fabs(desired_horizontal_velocity);
      scalar theta_d = acos(exp(-v_d/GAMMA));
      if (desired_horizontal_velocity < 0) theta_d = -theta_d;
      scalar stanceTime = 2*PI*sqrt(m_h / ls_k);

      scalar dtheta, theta_p;

      // PID controller ??
      Vector2s nn = n;
      scalar theta_hat = theta_c;
      for (int i = 0; i < 100; ++i) {
        scalar lmin = l0 - sqrt(m_h / ls_k) * fabs(head_vel.dot(nn));
        Vector2s v_t = head_vel - head_vel.dot(nn) * nn;
        scalar omegaAvg = 2 * v_t.norm() / (l0 + lmin);
        scalar sweepAngle = omegaAvg * stanceTime;
        // if (v_t(0) < 0) sweepAngle = -sweepAngle; // ??
        theta_p = theta_hat + sweepAngle;
        dtheta = theta_d - theta_p;
        if(fabs(dtheta) < 0.01) break;
        else {
          theta_hat += dtheta * 0.2;
          nn << -sin(theta_hat), -cos(theta_hat);
        }
      }

      theta_hat = theta_hat - 0.1;
      dtheta = theta_hat - theta_c;
      std::cout << "dtheta (PI): " << dtheta / PI << std::endl;
      while (dtheta > PI) dtheta -= PI; // %??
      while (dtheta < -PI) dtheta += PI;

      // Compute v_tan
      Vector2s v_h = head_vel - foot_vel;
      scalar vLeg = v_h.dot(n);
      Vector2s v_tan = v_h - vLeg * n;

      scalar coeff = 200; // tweak
      if ((dtheta < 0 && v_tan[0] * (-n[1]) < 0) || (dtheta > 0 && v_tan[0] * (-n[1]) > 0))
        coeff = 11; // tweak
      tau = -coeff * dtheta;

    std::cout << "theta_c: " << theta_c << std::endl;
    std::cout << "dtheta: " << dtheta << std::endl;
    std::cout << "tau: " << tau << std::endl;
    std::cout << "theta_p: " << theta_p << std::endl;
    std::cout << "theta_d: " << theta_d << std::endl;
    }

    if (velocity_detector->success()) ++vit;
  }


  /* Landing */
  static bool chooseTarget = true;
  static int isOnTheLeftOfTarget;
  static scalar target;
  static scalar speed_limit = 1;
  std::cout << "vel_h: " << head_vel(0) << std::endl;

  if (lit != landing_detectors.end() && T > 3) {
    const LandingDetector* landing_detector = *lit;
    scalar min_landing_zone_x = landing_detector->xmin();
    scalar max_landing_zone_x = landing_detector->xmax();
    // scalar desired_landing_zone_x = (min_landing_zone_x + max_landing_zone_x) / 2;

    scalar desired_horizontal_velocity;
    // if (T < 10) desired_horizontal_velocity = 0;
    // else {
    scalar dist;
    if (isOnTheLeftOfTarget) {
      if (head_pos(0) > target) chooseTarget = true;
    } else {
      if (head_pos(0) < target) chooseTarget = true;
    }

    if (chooseTarget) {
      if (fabs(max_landing_zone_x - head_pos(0)) > fabs(min_landing_zone_x - head_pos(0))) {
        target = max_landing_zone_x; isOnTheLeftOfTarget = true;
      } else { target = min_landing_zone_x; isOnTheLeftOfTarget = false; }
      chooseTarget = false;
    }
    dist = target - head_pos(0);
    desired_horizontal_velocity = dist*0.2; //* exp(-fabs(dist)-1);
    if (desired_horizontal_velocity > speed_limit) desired_horizontal_velocity = speed_limit;
    else if (desired_horizontal_velocity < -speed_limit) desired_horizontal_velocity = -speed_limit;
    std::cout << "vel_d: " << desired_horizontal_velocity << std::endl;

    if (fabs(dist) < 0.5) desired_horizontal_velocity = dist*0.1;
    // }

    if (foot_pos(1) - foot->getRadius() > 0.05) {
      // copy
      Vector2s n = foot_pos - head_pos;
      n /= n.norm();
      scalar theta_c = asin(-n(0));

      scalar v_d = fabs(desired_horizontal_velocity);
      scalar theta_d = acos(exp(-v_d/GAMMA));
      if (desired_horizontal_velocity < 0) theta_d = -theta_d;
      scalar stanceTime = 2*PI*sqrt(m_h / ls_k);

      scalar dtheta, theta_p;

      // PID controller ??
      Vector2s nn = n;
      scalar theta_hat = theta_c;
      for (int i = 0; i < 100; ++i) {
        scalar lmin = l0 - sqrt(m_h / ls_k) * fabs(head_vel.dot(nn));
        Vector2s v_t = head_vel - head_vel.dot(nn) * nn;
        scalar omegaAvg = 2 * v_t.norm() / (l0 + lmin);
        scalar sweepAngle = omegaAvg * stanceTime;
        // if (v_t(0) < 0) sweepAngle = -sweepAngle; // ??
        theta_p = theta_hat + sweepAngle;
        dtheta = theta_d - theta_p;
        if(fabs(dtheta) < 0.01) break;
        else {
          theta_hat += dtheta * 0.2;
          nn << -sin(theta_hat), -cos(theta_hat);
        }
      }

      theta_hat = theta_hat - 0.1;
      dtheta = theta_hat - theta_c;
      std::cout << "dtheta (PI): " << dtheta / PI << std::endl;
      while (dtheta > PI) dtheta -= PI; // %??
      while (dtheta < -PI) dtheta += PI;

      // Compute v_tan
      Vector2s v_h = head_vel - foot_vel;
      scalar vLeg = v_h.dot(n);
      Vector2s v_tan = v_h - vLeg * n;

      scalar coeff = 1000; // tweak
      if ((dtheta < 0 && v_tan[0] * (-n[1]) < 0) || (dtheta > 0 && v_tan[0] * (-n[1]) > 0))
        coeff = 11; // tweak
      tau = -coeff * dtheta;

    std::cout << "theta_c: " << theta_c << std::endl;
    std::cout << "dtheta: " << dtheta << std::endl;
    std::cout << "tau: " << tau << std::endl;
    std::cout << "theta_p: " << theta_p << std::endl;
    std::cout << "theta_d: " << theta_d << std::endl;
      // paste
    }


    if (landing_detector->success()) ++lit;
  }


  action.legSpringRestLength = l0;
  action.legTorque = tau;

  // In order to satisfy the detectors, you need to extract relevant
  // information from each detector above.
  //
  // In particular, you can see if a detector has already been satisfied by
  // calling its 'success()' method, which returns a boolean.
  //
  // For the info needed to satisfy a particular detector, read the detector
  // definitions in JointMotorBodySimulationJudge.h carefully. Some of the
  // most useful information includes:
  //

  if (treat_detectors.size() > 0)
  {
    int treat_rigid_body_index = treat_detectors[0]->treatrb();
    Vector2s treat_current_position = scene->getRigidBody(treat_detectors[0]->treatrb())->getX();
  }

  return action;
}
