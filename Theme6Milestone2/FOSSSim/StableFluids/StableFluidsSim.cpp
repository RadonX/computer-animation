#include "StableFluidsSim.h"
#include <Eigen/LU>

//#define VERBOSE (0)

StableFluidsSim::StableFluidsSim( const int& rows, const int& cols, const scalar& diff, const scalar& visc)
: m_diff(diff)
, m_visc(visc)
, m_N(rows)
, m_d(m_N + 2, m_N + 2)
, m_u(m_N + 2, m_N + 1)
, m_v(m_N + 1, m_N + 2)
, m_uAfterDiffusion(m_N + 2, m_N + 1)
, m_vAfterDiffusion(m_N + 1, m_N + 2)
, m_uAfterAdvect(m_N + 2, m_N + 1)
, m_vAfterAdvect(m_N + 1, m_N + 2)
, VERBOSE(false)
, m_all_ones(m_N + 2, m_N + 2)
{
    assert(rows==cols);
    
    clear();
}

StableFluidsSim::~StableFluidsSim()
{

}

////////////////////////////////////////////////////////////////////////////////////
void StableFluidsSim::SWAP(ArrayXs *& x1, ArrayXs *& x2)
{
    ArrayXs * t = x1;
    x1 = x2;
    x2 = t;
}

void StableFluidsSim::add_source(int N, ArrayXs * x, ArrayXs * x0, scalar dt)
{
    // nothing to do. adding marker/velocity is handled in StableFluidsEnsemble.
    *x = *x0;
}

void StableFluidsSim::diffuseD(int N, ArrayXs * x, ArrayXs * x0, scalar diff, scalar dt)
{
    assert((*x0 == *x0).all());
    
    scalar a = diff * dt * N * N;
    *x = *x0;
    
    for (int k = 0; k < 20; k++)
    {
        for (int i = 1; i <= N; i++)
        {
            for (int j = 1; j <= N; j++)
            {
                (*x)(i, j) = ((*x0)(i, j) + a * ((*x)(i, j + 1) + (*x)(i, j - 1) + (*x)(i + 1, j) + (*x)(i - 1, j))) / (1 + 4 * a);
            }
        }
    }
}

void StableFluidsSim::diffuseU(int N, ArrayXs * x, ArrayXs * x0, scalar diff, scalar dt)
{
    assert((*x0 == *x0).all());
    
    scalar a = diff * dt * N * N;
    *x = *x0;
    
    for (int k = 0; k < 20; k++)
    {
        for (int i = 1; i <= N; i++)
        {
            for (int j = 0; j <= N; j++)
            {
                if (j == 0)
                    (*x)(i, j) = ((*x0)(i, j) + a * ((*x)(i, j + 1) + (*x)(i + 1, j) + (*x)(i - 1, j))) / (1 + 3 * a);
                else if (j == N)
                    (*x)(i, j) = ((*x0)(i, j) + a * ((*x)(i, j - 1) + (*x)(i + 1, j) + (*x)(i - 1, j))) / (1 + 3 * a);
                else
                    (*x)(i, j) = ((*x0)(i, j) + a * ((*x)(i, j + 1) + (*x)(i, j - 1) + (*x)(i + 1, j) + (*x)(i - 1, j))) / (1 + 4 * a);
            }
        }
    }
}

void StableFluidsSim::diffuseV(int N, ArrayXs * x, ArrayXs * x0, scalar diff, scalar dt)
{
    assert((*x0 == *x0).all());
    
    scalar a = diff * dt * N * N;
    *x = *x0;
    
    for (int k = 0; k < 20; k++)
    {
        for (int i = 0; i <= N; i++)
        {
            for (int j = 1; j <= N; j++)
            {
                if (i == 0)
                    (*x)(i, j) = ((*x0)(i, j) + a * ((*x)(i, j + 1) + (*x)(i, j - 1) + (*x)(i + 1, j))) / (1 + 3 * a);
                else if (i == N)
                    (*x)(i, j) = ((*x0)(i, j) + a * ((*x)(i, j + 1) + (*x)(i, j - 1) + (*x)(i - 1, j))) / (1 + 3 * a);
                else
                    (*x)(i, j) = ((*x0)(i, j) + a * ((*x)(i, j + 1) + (*x)(i, j - 1) + (*x)(i + 1, j) + (*x)(i - 1, j))) / (1 + 4 * a);
            }
        }
    }
}

void StableFluidsSim::advectD(int N, ArrayXs * x, ArrayXs * x0, ArrayXs * u, ArrayXs * v, scalar dt)
{
    assert((*x0 == *x0).all());
    assert((*u == *u).all());
    assert((*v == *v).all());
    
    scalar xoffset = 0;
    scalar yoffset = 0;
    for (int i = 1; i <= N; i++)
    {
        for (int j = 1; j <= N; j++)
        {
            scalar bi = i + yoffset - interpolateV(v, i + yoffset, j + xoffset) * dt * N;
            scalar bj = j + xoffset - interpolateU(u, i + yoffset, j + xoffset) * dt * N;
            
            (*x)(i, j) = interpolateD(x0, bi, bj);
        }
    }
}

void StableFluidsSim::advectU(int N, ArrayXs * x, ArrayXs * x0, ArrayXs * u, ArrayXs * v, scalar dt)
{
    assert((*x0 == *x0).all());
    assert((*u == *u).all());
    assert((*v == *v).all());
    
    scalar xoffset = 0.5;
    scalar yoffset = 0;
    for (int i = 1; i <= N; i++)
    {
        for (int j = 0; j <= N; j++)
        {
            scalar bi = i + yoffset - interpolateV(v, i + yoffset, j + xoffset) * dt * N;
            scalar bj = j + xoffset - interpolateU(u, i + yoffset, j + xoffset) * dt * N;
            
            (*x)(i, j) = interpolateU(x0, bi, bj);
        }
    }
}

void StableFluidsSim::advectV(int N, ArrayXs * x, ArrayXs * x0, ArrayXs * u, ArrayXs * v, scalar dt)
{
    assert((*x0 == *x0).all());
    assert((*u == *u).all());
    assert((*v == *v).all());
    
    scalar xoffset = 0;
    scalar yoffset = 0.5;
    for (int i = 0; i <= N; i++)
    {
        for (int j = 1; j <= N; j++)
        {
            scalar bi = i + yoffset - interpolateV(v, i + yoffset, j + xoffset) * dt * N;
            scalar bj = j + xoffset - interpolateU(u, i + yoffset, j + xoffset) * dt * N;
            
            (*x)(i, j) = interpolateV(x0, bi, bj);
        }
    }
}

void StableFluidsSim::project(int N, ArrayXs * u, ArrayXs * v, ArrayXs * u0, ArrayXs * v0)
{
    if (VERBOSE) std::cout << "u0: " << std::endl << *u0 << std::endl << std::endl;
    if (VERBOSE) std::cout << "v0: " << std::endl << *v0 << std::endl << std::endl;
    
    ArrayXs div(N + 2, N + 2);
    ArrayXs p(N + 2, N + 2);
    div.setZero();
    p.setZero();
    scalar h = 1.0 / N;
    
    // set solid boundary conditions
    for (int i = 0; i <= N + 1; i++)
    {
        (*u0)(i, 0) = 0;
        (*u0)(i, N) = 0;
        (*v0)(0, i) = 0;
        (*v0)(N, i) = 0;
    }
    
    for (int i = 0; i <= N; i++)
    {
        (*u0)(0, i) = 0;
        (*u0)(N + 1, i) = 0;
        (*v0)(i, 0) = 0;
        (*v0)(i, N + 1) = 0;
    }
        
    // compute divergence of the velocity field
    for (int i = 1; i <= N; i++)
    {
        for (int j = 1; j <= N; j++)
        {
            div(i, j) = ((*v0)(i, j) - (*v0)(i - 1, j) + (*u0)(i, j) - (*u0)(i, j - 1)) / h;
        }
    }
    if (VERBOSE) std::cout << "pre projection div: " << std::endl << div << std::endl << std::endl;
    
    // solve for pressure
    for (int k = 0; k < 20; k++)
    {
        for (int i = 1; i <= N; i++)
        {
            for (int j = 1; j <= N; j++)
            {
                scalar numer = div(i, j) * (h * h);
                int denom = -4;
                
                if (i == 1) denom++;
                else        numer -= p(i - 1, j);
                if (i == N) denom++;
                else        numer -= p(i + 1, j);
                if (j == 1) denom++;
                else        numer -= p(i, j - 1);
                if (j == N) denom++;
                else        numer -= p(i, j + 1);

                assert(denom != 0);
                p(i, j) = numer / denom;
            }
        }
    }
    if (VERBOSE) std::cout << "p: " << std::endl << p << std::endl << std::endl;
    
    // apply pressure to correct velocities
    (*u) = (*u0);
    (*v) = (*v0);
    for (int i = 1; i <= N; i++)
    {
        for (int j = 1; j < N; j++)
        {
            (*u)(i, j) = (*u0)(i, j) - (p(i, j + 1) - p(i, j)) / h;
            (*v)(j, i) = (*v0)(j, i) - (p(j + 1, i) - p(j, i)) / h;
        }
    }
    if (VERBOSE) std::cout << "final u: " << std::endl << *u << std::endl << std::endl;
    if (VERBOSE) std::cout << "final v: " << std::endl << *v << std::endl << std::endl;
    
    // compute divergence of the velocity field
    for (int i = 1; i <= N; i++)
    {
        for (int j = 1; j <= N; j++)
        {
            div(i, j) = ((*v)(i, j) - (*v)(i - 1, j) + (*u)(i, j) - (*u)(i, j - 1)) / h;
        }
    }    
    if (VERBOSE) std::cout << "post projection divergence: " << std::endl << div << std::endl << std::endl;
    
}

void StableFluidsSim::dens_step(int N, ArrayXs * x, ArrayXs * x0, ArrayXs * u, ArrayXs * v, scalar diff, scalar dt)
{
    ArrayXs * outu = u;
    ArrayXs * outv = v;
    
    add_source(N, x, x0, dt); 
    
    SWAP(x0, x); 
    diffuseD(N, x, x0, diff, dt); 
    
    SWAP(x0, x); 
    advectD(N, x, x0, u, v, dt);
    
    if (outu != u)
        *outu = *u;    
    if (outv != v)
        *outv = *v;
    
}

void StableFluidsSim::vel_step(int N, ArrayXs * u, ArrayXs * v, ArrayXs * u0, ArrayXs * v0, scalar visc, scalar dt)
{
    ArrayXs * outu = u;
    ArrayXs * outv = v;
    
    add_source(N, u, u0, dt); 
    add_source(N, v, v0, dt); 
  
    m_uAfterDiffusion.setZero();
    m_vAfterDiffusion.setZero();
    m_uAfterDiffusion = *u;
    m_vAfterDiffusion = *v;
  
    // diffusion
//    SWAP(u0, u); 
//    SWAP(v0, v); 
//    diffuseU(N, u, u0, visc, dt); 
//    diffuseV(N, v, v0, visc, dt); 
    
//    SWAP(u0, u);
//    SWAP(v0, v);
//    project(N, u, v, u0, v0);
    
    // advection
    SWAP(u0, u); 
    SWAP(v0, v); 
    advectU(N, u, u0, u0, v0, dt); 
    advectV(N, v, v0, u0, v0, dt); 

    m_uAfterAdvect.setZero();
    m_vAfterAdvect.setZero();
    m_uAfterAdvect = *u;
    m_vAfterAdvect = *v;
  
    SWAP(u0, u);
    SWAP(v0, v);
    project(N, u, v, u0, v0);
    
    if (outu != u)
        *outu = *u;    
    if (outv != v)
        *outv = *v;
    
}

void StableFluidsSim::stepSystem( const scalar& dt)
{
    if (VERBOSE) std::cout << "step" << std::endl;
    
    ArrayXs new_d(m_N + 2, m_N + 2);
    ArrayXs new_u(m_N + 2, m_N + 1);
    ArrayXs new_v(m_N + 1, m_N + 2);
    
    new_d.setZero();
    new_u.setZero();
    new_v.setZero();
    
    vel_step(m_N, &new_u, &new_v, &m_u, &m_v, m_visc, dt);
    dens_step(m_N, &new_d, &m_d, &new_u, &new_v, m_diff, dt);
    
    m_d = new_d;
    m_u = new_u;
    m_v = new_v;
}

const ArrayXs& StableFluidsSim::getMarkerDensities() const
{
    return m_d;
}

ArrayXs& StableFluidsSim::getMarkerDensities()
{
    return m_d;
}

const ArrayXs& StableFluidsSim::getHorizontalVelocities() const
{
  return m_u;
}

const ArrayXs& StableFluidsSim::getVerticalVelocities() const
{
  return m_v;
}

ArrayXs& StableFluidsSim::getHorizontalVelocities()
{
    return m_u;
}

ArrayXs& StableFluidsSim::getVerticalVelocities()
{
    return m_v;
}

int StableFluidsSim::physicalRows() const
{
    return m_N;
}

int StableFluidsSim::physicalCols() const
{
    return m_N;
}

void StableFluidsSim::clear()
{
    m_d.setZero();
    m_u.setZero();
    m_v.setZero();
    m_all_ones.setOnes();
}


scalar StableFluidsSim::interpolateD(ArrayXs * d, scalar i, scalar j)
{
    int i1 = CLAMP((int)i, 0, m_N);
    int i2 = i1 + 1;
    
    int j1 = CLAMP((int)j, 0, m_N);
    int j2 = j1 + 1;
    
    scalar s = CLAMP(i - i1, 0, 1);
    scalar t = CLAMP(j - j1, 0, 1);
    
    return ((*d)(i1, j1) * (1 - s) + (*d)(i2, j1) * s) * (1 - t) + 
           ((*d)(i1, j2) * (1 - s) + (*d)(i2, j2) * s) * t;
}

scalar StableFluidsSim::interpolateU(ArrayXs * u, scalar i, scalar j)
{
    int i1 = CLAMP((int)i, 0, m_N);
    int i2 = i1 + 1;
    
    int j1 = CLAMP((int)(j - 0.5), 0, m_N - 1);
    int j2 = j1 + 1;
    
    scalar s = CLAMP(i - i1, 0, 1);
    scalar t = CLAMP(j - 0.5 - j1, 0, 1);
    
    return ((*u)(i1, j1) * (1 - s) + (*u)(i2, j1) * s) * (1 - t) + 
           ((*u)(i1, j2) * (1 - s) + (*u)(i2, j2) * s) * t;
}

scalar StableFluidsSim::interpolateV(ArrayXs * v, scalar i, scalar j)
{
    int i1 = CLAMP((int)(i - 0.5), 0, m_N - 1);
    int i2 = i1 + 1;
    
    int j1 = CLAMP((int)j, 0, m_N);
    int j2 = j1 + 1;
    
    scalar s = CLAMP(i - 0.5 - i1, 0, 1);
    scalar t = CLAMP(j - j1, 0, 1);
    
    return ((*v)(i1, j1) * (1 - s) + (*v)(i2, j1) * s) * (1 - t) + 
           ((*v)(i1, j2) * (1 - s) + (*v)(i2, j2) * s) * t;
}

void StableFluidsSim::setPrescribedVelocity(int p)
{
//    m_u.setZero();
//    m_v.setZero();
    
    switch (p)
    {
        case 0:
            break;
        case 1:
            for (int i = m_N * 0.2; i <= m_N * 0.8; i++)
                for (int j = m_N * 0.2; j <= m_N * 0.8; j++)
                    m_u(i, j) = m_v(i, j) = 0.8;
            break;
        case 2:
            for (int i = m_N * 0.2; i <= m_N * 0.8; i++)
                m_u(i, i) = m_v(i, i) = 0.8;
            break;
        case 3:
            for (int i = m_N * 0.2; i <= m_N * 0.8; i++)
                m_v(i, m_N * 0.2 + 1) = -1.6,
                m_v(i, m_N * 0.8 + 1) = -1.6;
            break;
        case 4:
            for (int i = m_N * 0.2; i <= m_N * 0.8; i++)
                m_u(m_N, i) = 2.4;
            break;
        case 5:
            for (int i = m_N * 0.2; i <= m_N * 0.8; i++)
                for (int j = m_N * 0.83; j <= m_N; j++)
                    m_u(j, i) = 2.4;
            break;
        case 8:
            for (int i = m_N * 0.2; i <= m_N * 0.8; i++)
                m_v(i, m_N / 2) = 0.8;
            break;
        case 9:
            for (int i = m_N * 0.2; i <= m_N * 0.8; i++)
                m_v(i, m_N / 2) = -0.8;
            break;
    }
}


void StableFluidsSim::copyState( const StableFluidsSim& otherscene )
{
  // IMPORTANT: DO NOT MODIFY THIS CODE!
  m_diff = otherscene.m_diff;
  m_visc = otherscene.m_visc;
  m_N = otherscene.m_N;
  m_d = otherscene.m_d;
  m_u = otherscene.m_u;
  m_v = otherscene.m_v;
}

ArrayXs& StableFluidsSim::getHorizontalVelocitiesAdvect()
{
  return m_uAfterAdvect;
}

const ArrayXs& StableFluidsSim::getHorizontalVelocitiesAdvect() const
{
  return m_uAfterAdvect;
}

ArrayXs& StableFluidsSim::getVerticalVelocitiesAdvect()
{
  return m_vAfterAdvect;
}

const ArrayXs& StableFluidsSim::getVerticalVelocitiesAdvect() const
{
  return m_vAfterAdvect;
}

ArrayXs& StableFluidsSim::getHorizontalVelocitiesDiffusion()
{
  return m_uAfterDiffusion;
}

const ArrayXs& StableFluidsSim::getHorizontalVelocitiesDiffusion() const
{
  return m_uAfterDiffusion;
}

ArrayXs& StableFluidsSim::getVerticalVelocitiesDiffusion()
{
  return m_vAfterDiffusion;
}

const ArrayXs& StableFluidsSim::getVerticalVelocitiesDiffusion() const
{
  return m_vAfterDiffusion;
}

ArrayXb& StableFluidsSim::getHasFluid()
{
  return m_all_ones;
}

const ArrayXb& StableFluidsSim::getHasFluid() const
{
  return m_all_ones;
}

void StableFluidsSim::setDiffusion(scalar diff)
{
  m_diff = diff;
}

void StableFluidsSim::setViscosity(scalar visc)
{
  m_visc = visc;
}

scalar StableFluidsSim::getDiffusion()
{
  return m_diff;
}

scalar StableFluidsSim::getViscosity()
{
  return m_visc;
}


