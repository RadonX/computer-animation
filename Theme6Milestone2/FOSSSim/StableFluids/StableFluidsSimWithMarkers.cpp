#include "StableFluidsSimWithMarkers.h"
#include <Eigen/LU>

#define VERBOSE (1)
#define SOLVER  (0)
#define VOLUME_COMPENSATION_ETA (0.01)
#define adjustP(i, j) ((m_has_fluid(i, j))? p0(i, j): 0.0)
#define setAirVel(X, flag, i, j) ((flag(i, j))? (tmp += X(i, j)): (--count))

StableFluidsSimWithMarkers::StableFluidsSimWithMarkers( const int& rows, const int& cols, const ArrayXb & has_solid, const scalar& diff, const scalar& visc)
: StableFluidsSim(rows, cols, diff, visc)
, m_has_fluid(m_N + 2, m_N + 2)
, m_has_solid(has_solid)
{
    assert(rows==cols);

    clear();
}

StableFluidsSimWithMarkers::~StableFluidsSimWithMarkers()
{

}

//////////////////////////////////////////////////////////////////////////////////////

bool StableFluidsSimWithMarkers::in_solid(const Vector2s & pos)
{
    int i = std::max(1, std::min(m_N, (int)(pos.x() * m_N + 0.5)));
    int j = std::max(1, std::min(m_N, (int)(pos.y() * m_N + 0.5)));
    return m_has_solid(i, j);
}

void StableFluidsSimWithMarkers::detect_fluid(int N, ArrayXb & fluids, const std::vector<Vector2s> & particles)
{
    fluids.setZero();
    for (size_t p = 0; p < particles.size(); p++)
    {
        int i = std::max(1, std::min(N, (int)(particles[p].x() * N + 0.5)));
        int j = std::max(1, std::min(N, (int)(particles[p].y() * N + 0.5)));
        fluids(i, j) = true;
    }
}

void StableFluidsSimWithMarkers::solveByJacobi(ArrayXs & p, ArrayXs & div)
{
  // Add Theme 6 Milestone 2 code here.
  // Implement the Jacobi Solver with Ping-pong buffers

  int N = m_N;
  scalar h = 1.0 / N;

  ArrayXs p_copy = p;

  ArrayXs* ps[2] = {&p, &p_copy};

  // solve for pressure inside the region ([1, N], [1, N])
  for (int k = 0; k < 500; k++)
  {
    for (int i = 1; i <= N; i++)
    {
      for (int j = 1; j <= N; j++)
      {
        ArrayXs &p0 = (*ps[(k & 1)]); // current state
        ArrayXs &p1 = (*ps[!(k & 1)]); // the next state
        if (!m_has_fluid(i, j)) {
            p1(i, j) = 0; // don't need adjustP !!
            continue;
        }

        int pnum = 4 - m_has_solid(i-1, j) - m_has_solid(i+1, j) - m_has_solid(i, j-1) - m_has_solid(i, j+1);
        scalar psum = adjustP(i-1, j) + adjustP(i+1, j) + adjustP(i, j-1) + adjustP(i, j+1); // p[solid or air] = 0
        if (pnum != 0)
          p1(i, j) = (psum - h*h * div(i, j)) / pnum;
      }
    }

  }
}

void StableFluidsSimWithMarkers::solveByGaussSeidel(ArrayXs & p, ArrayXs & div)
{
    int N = m_N;
    scalar h = 1.0 / N;

    for (int k = 0; k < 500; k++)
    {
        for (int i = 1; i <= N; i++)
        {
            for (int j = 1; j <= N; j++)
            {
                if (m_has_solid(i, j) || !m_has_fluid(i, j))
                {
                    p(i, j) = 0;
                    continue;
                }

                scalar numer = div(i, j) * (h * h);
                int denom = -4;

                if (m_has_solid(i - 1, j))      denom++;
                else if (m_has_fluid(i - 1, j)) numer -= p(i - 1, j);
                if (m_has_solid(i + 1, j))      denom++;
                else if (m_has_fluid(i + 1, j)) numer -= p(i + 1, j);
                if (m_has_solid(i, j - 1))      denom++;
                else if (m_has_fluid(i, j - 1)) numer -= p(i, j - 1);
                if (m_has_solid(i, j + 1))      denom++;
                else if (m_has_fluid(i, j + 1)) numer -= p(i, j + 1);

                assert(denom != 0);
                p(i, j) = numer / denom;
            }
        }
    }
}

void StableFluidsSimWithMarkers::solveByEigenLU(ArrayXs & p, ArrayXs & div)
{
    int N = m_N;
    scalar h = 1.0 / N;

    MatrixXs A = MatrixXs::Zero(N * N, N * N);
    VectorXs b = VectorXs::Zero(N * N);

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (m_has_solid(i + 1, j + 1))
            {
                // solid cell
                A(i * N + j, i * N + j) = 1;
                b(i * N + j) = 0;

            } else if (!m_has_fluid(i + 1, j + 1))
            {
                // air cell
                A(i * N + j, i * N + j) = 1;
                b(i * N + j) = 0;

            } else
            {
                // fluid cell
                b(i * N + j) = div(i + 1, j + 1) / (N * N);
                A(i * N + j, i * N + j) = -4;

                if      (m_has_solid(i + 2, j + 1)) A(i * N + j, i * N + j)++;
                else if (m_has_fluid(i + 2, j + 1)) A(i * N + j, (i + 1) * N + j) = 1;
                if      (m_has_solid(i + 0, j + 1)) A(i * N + j, i * N + j)++;
                else if (m_has_fluid(i + 0, j + 1)) A(i * N + j, (i - 1) * N + j) = 1;
                if      (m_has_solid(i + 1, j + 2)) A(i * N + j, i * N + j)++;
                else if (m_has_fluid(i + 1, j + 2)) A(i * N + j, i * N + (j + 1)) = 1;
                if      (m_has_solid(i + 1, j + 0)) A(i * N + j, i * N + j)++;
                else if (m_has_fluid(i + 1, j + 0)) A(i * N + j, i * N + (j - 1)) = 1;

            }
        }
    }

    VectorXs pv = A.fullPivLu().solve(b);

    for (int i = 1; i <= N; i++)
        for (int j = 1; j <= N; j++)
            p(i, j) = pv((i - 1) * N + (j - 1));
}

void StableFluidsSimWithMarkers::solveByCG(ArrayXs & p, ArrayXs & div)
{

}

void StableFluidsSimWithMarkers::solveByPCG(ArrayXs & p, ArrayXs & div)
{

}

void StableFluidsSimWithMarkers::project(int N, ArrayXs * u, ArrayXs * v, ArrayXs * u0, ArrayXs * v0)
{
  //////////////////////////////////
  // Add Theme 6 Milestone 2 code here.

  ArrayXs div(N + 2, N + 2);
  ArrayXs p(N + 2, N + 2);
  scalar h = 1.0 / N;

  ArrayXs &V0 = (*v0);
  ArrayXs &U0 = (*u0);

  // set velocities on solid boundaries to zero
  // ( I think boundary of u0, v0 should be set in the very beginning of the class)
  for (int i = 0; i <= N + 1; i++)
  {
    for (int j = 0; j <= N + 1; j++)
    {
      // Solid cell has no fluid. We set has_fluid as false so that adjustP returns 0.0.
      if (m_has_solid(i, j)) m_has_fluid(i, j) = false; // Can do this in the beginning of the class

      if ((j <= N) && (m_has_solid(i, j) || (m_has_solid(i, j+1)))) U0(i, j) = 0;
      if ((i <= N) && (m_has_solid(i, j) || (m_has_solid(i+1, j)))) V0(i, j) = 0;
    }
  }

  // compute the discrete divergence of the velocity field,
  // with modifications to compensate for volume change.
  div.setZero(); // don't need this
  for (int i = 1; i <= N; i++)
  {
    for (int j = 1; j <= N; j++)
    {
      div(i, j) = (U0(i, j) - U0(i, j-1) + V0(i, j) - V0(i-1, j)) / h
          + VOLUME_COMPENSATION_ETA * (fluid_cell_count() - m_original_fluid_cell_count);
    }
  }
  // if (VERBOSE) std::cout << div << std::endl;

  // solve for pressure
  p.setZero(); // Always load P using adjustP()
  solveByJacobi(p, div); // div(i,j) is only applied to p(i,j)
  // if (VERBOSE) std::cout << "p: " << std::endl << p << std::endl << std::endl;

  ArrayXs &U = (*u);
  ArrayXs &V = (*v);
  ArrayXb flagU(m_N + 2, m_N + 1), newFlagU(m_N + 2, m_N + 1);
  ArrayXb flagV(m_N + 1, m_N + 2), newFlagV(m_N + 1, m_N + 2);
  flagU.setZero(); flagV.setZero();

  U = U0;
  V = V0;
  // /*
  // apply the pressure gradient to the velocity field
  // ([1, N], [1, N-1)) for u, ([1, N-1), [1, N]) for v
  for (int i = 1; i <= N; i++)
  {
    for (int j = 1; j < N; j++)
    {
      // step 0: Tag all the velocity samples set by the pressure solve to valid
      U(i, j) -= (p(i, j+1) - p(i, j)) / h; flagU(i, j) = true;
      V(j, i) -= (p(j+1, i) - p(j, i)) / h; flagV(j, i) = true;
    //   if (i != N) {
    //     V(i, j) -= (p(i+1, j) - p(i, j)) / h; // V is row major, so don't use V(j,i)
    //     flagV(i, j) = true;
    //   }
    }
    // if (i != N) {
    //   V(i, N) -= (p(i+1, N) - p(i, N)) / h;
    //   flagV(i, N) = true;
    // }
  }

  // set solid boundary conditions again to overwrite the wrong results
  // coming from applying pressures above
  for (int i = 0; i <= N + 1; i++)
  {
    for (int j = 0; j <= N + 1; j++)
    {

      // step 1: test if both adjacent cells are air-cell;
      // if so, set velocity to a sentinel value (SCALAR_INFINITY)
      if (j <= N) {
        if (m_has_solid(i, j) || m_has_solid(i, j+1)){
          U(i, j) = 0; flagU(i, j) = false; // ignore velocity of boundary neighbor ??
        } else if (!(m_has_fluid(i, j) || m_has_fluid(i, j+1))){
          U(i, j) = SCALAR_INFINITY; flagU(i, j) = false;
        }
      }
      if (i <= N) {
        if (m_has_solid(i, j) || m_has_solid(i+1, j)) {
          V(i, j) = 0; flagV(i, j) = false; // ignore velocity of boundary neighbor
        } else if (!(m_has_fluid(i, j) || m_has_fluid(i+1, j))) {
          V(i, j) = SCALAR_INFINITY; flagV(i, j) = false;
        }
      }
    }
  }
  // */

  /* velocity extrapolation:
    propagate liquid velocities to nearby air cells, up to 5 layers */

  // step 2: if the velocity of current cell is SCALAR_INFINITY,
  // propagate all the neighbor cells that don't have a SCALAR_INFINITY to
  // current cell and average them by dividing the number of propagated cells.

  // but is yet to correct the data leak bug during extrapolation (the averaging is done without using a buffer).
  // https://piazza.com/class/isicmq8145i5kt?cid=493
  // /*
  // ArrayXb *flagu0, *flagu1, *flagv0, *flagv1; // don't want to implement swap now !!
  newFlagU = flagU; // newFlag updated on the basis of the old one
  newFlagV = flagV;
  for (int k = 0; k < 5; k++)
  {
    for (int i = 1; i <= N; i++)
    {
      for (int j = 1; j < N; j++)
      {
        // int ip = CLAMP(i + 1, 1, N); // Why clamp ??
        // int im = CLAMP(i - 1, 1, N);
        // int jp = CLAMP(j + 1, 1, N - 1);
        // int jm = CLAMP(j - 1, 1, N - 1);
        int ip = i+1, im = i-1, jp = j+1, jm = j-1;

        if (U(i, j) == SCALAR_INFINITY) { // air cell without velocity set
          scalar tmp = 0;
          int count = 4;
          setAirVel(U, flagU, im, j);
          setAirVel(U, flagU, ip, j);
          setAirVel(U, flagU, i, jm);
          setAirVel(U, flagU, i, jp);
          // Set each invalid value having at least one valid neighbour
          // to the average of its valid neighbours
          if (count != 0) {
            U(i, j) = tmp / count;
            newFlagU(i, j) = true; // tag each of the values you changed as valid
          }

        }
        if (V(j, i) == SCALAR_INFINITY) {
          scalar tmp = 0;
          int count = 4;
          setAirVel(V, flagV, jm, i);
          setAirVel(V, flagV, jp, i);
          setAirVel(V, flagV, j, im);
          setAirVel(V, flagV, j, ip);
          if (count != 0) {
            //   V(j, i) = 2;
            V(j, i) = tmp / count;
            // if (VERBOSE) std::cout << V(j, i) << std::endl;
            newFlagV(j, i) = true;
          }
        }
      }
    }
    flagU = newFlagU;
    flagV = newFlagV;
  }
  // */

  // step 3: replace all the remaining velocities = SCALAR_INFINITY by 0.
  for (int i = 1; i <= N; i++)
  {
    for (int j = 1; j < N; j++)
    {
      if (U(i, j) == SCALAR_INFINITY) U(i, j) = 0;
      if (V(j, i) == SCALAR_INFINITY) V(j, i) = 0;
    //   if (V(i, j) == SCALAR_INFINITY) V(i, j) = 0;
    }
    // if (V(i, N) == SCALAR_INFINITY) V(i, N) = 0;
  }
}

void StableFluidsSimWithMarkers::dens_step(int N, ArrayXs * x, ArrayXs * x0, ArrayXs * u, ArrayXs * v, scalar diff, scalar dt)
{
    ArrayXs * outu = u;
    ArrayXs * outv = v;

    add_source(N, x, x0, dt);

    SWAP(x0, x);
    diffuseD(N, x, x0, diff, dt);

    SWAP(x0, x);
    advectD(N, x, x0, u, v, dt);

    if (outu != u)
        *outu = *u;
    if (outv != v)
        *outv = *v;

}

void StableFluidsSimWithMarkers::vel_step(int N, ArrayXs * u, ArrayXs * v, ArrayXs * u0, ArrayXs * v0, scalar visc, scalar dt)
{
    ArrayXs * outu = u;
    ArrayXs * outv = v;

    add_source(N, u, u0, dt);
    add_source(N, v, v0, dt);

    // gravity
    SWAP(u0, u);
    SWAP(v0, v);
    scalar g = 10.0;
    *u = *u0;
    *v = *v0 + g * dt;

    SWAP(u0, u);
    SWAP(v0, v);
    project(N, u, v, u0, v0);

    m_uAfterDiffusion.setZero();
    m_vAfterDiffusion.setZero();
    m_uAfterDiffusion = *u;
    m_vAfterDiffusion = *v;
    // diffusion
//    SWAP(u0, u);
//    SWAP(v0, v);
//    diffuseU(N, u, u0, visc, dt);
//    diffuseV(N, v, v0, visc, dt);

//    SWAP(u0, u);
//    SWAP(v0, v);
//    project(N, u, v, u0, v0);

    // advection
    SWAP(u0, u);
    SWAP(v0, v);
    advectU(N, u, u0, u0, v0, dt);
    advectV(N, v, v0, u0, v0, dt);

    m_uAfterAdvect.setZero();
    m_vAfterAdvect.setZero();
    m_uAfterAdvect = *u;
    m_vAfterAdvect = *v;

    SWAP(u0, u);
    SWAP(v0, v);
    project(N, u, v, u0, v0);

    if (outu != u)
        *outu = *u;
    if (outv != v)
        *outv = *v;

}

//////////////////////////////////////////////////////////////////////////////////////

void StableFluidsSimWithMarkers::advect_particles(int N, ArrayXs & u, ArrayXs & v, scalar dt)
{
    std::vector<Vector2s> & particles = m_particles;

    //////////////////////////////////
    // Add Theme 6 Milestone 2 code here.
    // Hint: Use interpolateV and interpolateU to get the new position of a particle
    // then CLAMP it to the range [0.5 / N, (N + 0.4999) / N]
    const double c_1 = 0.5;
    const double c_2 = 0.4999;
    const scalar lower = c_1 / N, upper = 1 + c_2 / N;

    // /*
    std::vector<Vector2s>::iterator it;
    for (it = m_particles.begin(); it != m_particles.end(); ++it) {
        Vector2s &x = *it;

        // interpolate to get velocity at position x.
        scalar ix = x(0) * N, jx = x(1) * N;
        scalar ux = interpolateU(&u, ix, jx);
        scalar vx = interpolateV(&v, ix, jx);
        // if (VERBOSE & vx > 0) std::cout << x << std::endl;
        // explicitly update the position using the interpolated velocity x.
        // clamp the updated position into the range [lower, upper]
        x = Vector2s(CLAMP(x(0) + vx * dt, lower, upper),
            CLAMP(x(1) + ux*dt, lower, upper));
    }
    // */

    detect_fluid(N, m_has_fluid, particles);
}

//////////////////////////////////////////////////////////////////////////////////////

void StableFluidsSimWithMarkers::stepSystem( const scalar& dt)
{
    if (VERBOSE) std::cout << "step" << std::endl;
    if (VERBOSE) std::cout << "fluid cell count = " << fluid_cell_count() << std::endl;

    ArrayXs new_d(m_N + 2, m_N + 2);
    ArrayXs new_u(m_N + 2, m_N + 1);
    ArrayXs new_v(m_N + 1, m_N + 2);

    new_d.setZero();
    new_u.setZero();
    new_v.setZero();

    vel_step(m_N, &new_u, &new_v, &m_u, &m_v, m_visc, dt);
    advect_particles(m_N, new_u, new_v, dt);

    dens_step(m_N, &new_d, &m_d, &new_u, &new_v, m_diff, dt);

    m_d = new_d;
    m_u = new_u;
    m_v = new_v;

}

void StableFluidsSimWithMarkers::clear()
{
    StableFluidsSim::clear();

    m_particles.clear();
    for (int i = m_N / 2; i <= m_N; i++)
    {
        for (int j = 1; j <= m_N; j++)
        {
            for (int k = 0; k < 4; k++)
            {
                if (!m_has_solid(i, j))
                {
                    m_particles.push_back(Vector2s(i + (double)rand() / RAND_MAX - 0.5, j + (double)rand() / RAND_MAX - 0.5) / m_N);
                }
            }
        }
    }

    detect_fluid(m_N, m_has_fluid, m_particles);
    m_original_fluid_cell_count = fluid_cell_count();
}
