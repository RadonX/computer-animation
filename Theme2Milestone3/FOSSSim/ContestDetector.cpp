#include "ContestDetector.h"
#include <iostream>
#include "TwoDScene.h"
#include <set>
// #include <thread>
#include <ctime>

#define DebugContest
// #define DebugContest2

// For all pairs of potentially-overlapping objects, applies the penalty force.
//
// Does not need to be modified by students.
void ContestDetector::performCollisionDetection(const TwoDScene &scene, const VectorXs &qs, const VectorXs &qe, DetectionCallback &dc)
{
  if( (qs-qe).norm() > 1e-8)
    {
      std::cerr << "Contest collision detector is only designed for use with the penalty method!" << std::endl;
      exit(1);
    }

  PPList pppairs;
  PEList pepairs;
  PHList phpairs;

  findCollidingPairs(scene, qe, pppairs, pepairs, phpairs);
  for(PPList::iterator it = pppairs.begin(); it != pppairs.end(); ++it)
    dc.ParticleParticleCallback(it->first, it->second);
  for(PEList::iterator it = pepairs.begin(); it != pepairs.end(); ++it)
    {
      //particle never collides with an edge it's an endpoint of
      if(scene.getEdge(it->second).first != it->first && scene.getEdge(it->second).second != it->first)
	dc.ParticleEdgeCallback(it->first, it->second);
    }
  for(PHList::iterator it = phpairs.begin(); it != phpairs.end(); ++it)
    dc.ParticleHalfplaneCallback(it->first, it->second);
}

// Given particle positions, computes lists of *potentially* overlapping object
// pairs. How exactly to do this is up to you.
// Inputs:
//   scene:  The scene object. Get edge information, radii, etc. from here. If
//           for some reason you'd also like to use particle velocities in your
//           algorithm, you can get them from here too.
//   x:      The positions of the particle.
// Outputs:
//   pppairs: A list of (particle index, particle index) pairs of potentially
//            overlapping particles. IMPORTANT: Each pair should only appear
//            in the list at most once. (1, 2) and (2, 1) count as the same
//            pair.
//   pepairs: A list of (particle index, edge index) pairs of potential
//            particle-edge overlaps.
//   phpairs: A list of (particle index, halfplane index) pairs of potential
//            particle-halfplane overlaps.

// Add Theme 2 Milestone 3 code here.
inline void projectPToX(const int n, const VectorXs &x, const std::vector<scalar> &r,
        std::vector<Interval1> &intervals, std::vector<SPair> &yInt) {
    intervals.reserve(n); yInt.reserve(n);
    for (int i = 0; i < n; ++i) {
        // todo: get [min, max]
        intervals.emplace_back(Interval1(x(2*i)-r[i], x(2*i)+r[i], i));
        yInt.emplace_back(std::make_pair(x(2*i+1)-r[i], x(2*i+1)+r[i]));
    }
}
template<int dim>
inline Interval1 makeEdgeInterval(const int idx, const int eidx1, const int eidx2,
        const scalar rEdge, const VectorXs &x) {
    if (x(2*eidx1+dim) < x(2*eidx2+dim)) {
        return Interval1(x(2*eidx1+dim)-rEdge, x(2*eidx2+dim)+rEdge, idx);
    } else {
        return Interval1(x(2*eidx2+dim)-rEdge, x(2*eidx1+dim)+rEdge, idx);
    }
}
inline void projectEToX(const int n, const std::vector<std::pair<int,int> > &edges, const std::vector<scalar> &r,
        std::vector<Interval1> &intervals, std::vector<SPair> &yInt,
        const VectorXs &x, std::vector<scalar> &rx) {
    intervals.reserve(n); yInt.reserve(n);
    for (int i = 0; i < n; ++i) {
        // todo: get [min, max]
        int eidx1 = edges[i].first; int eidx2 = edges[i].second;
        rx[eidx1] = std::max(r[i], rx[eidx1]);
        rx[eidx2] = std::max(r[i], rx[eidx2]);
        intervals.emplace_back(makeEdgeInterval<0>(i, eidx1, eidx2, r[i], x));
        Interval1 yinterval = makeEdgeInterval<1>(i, eidx1, eidx2, r[i], x);
        yInt.emplace_back(std::make_pair(yinterval.m, yinterval.M));
    }
}
inline bool beNotOverlap(const SPair &a, const SPair &b) {
    if (a.second < b.first || b.second < a.first) return true;
    else return false;
}
inline bool beNotOverlap(const Interval1 &a, const SPair &b) {
    if (a.M < b.first || b.second < a.m) return true;
    else return false;
}
inline void sweepPP(int i, TmpList &pplist, const int n,
        const std::vector<Interval1> &intervals, const std::vector<SPair> &yInt) {

    // todo: i think i can have an additional set<set> to reuse previous set

    int idx1 = intervals[i].id;
    const SPair &yinterval = yInt[idx1];
    for (int j = i + 1; j < n; ++j) {
        // will Interval_j overlap with Interval_i
        // mi <= mj
        if (intervals[i].M >= intervals[j].m){  // ==
            // check if they possibly overlap
            int idx2 = intervals[j].id;
            if (!beNotOverlap(yinterval, yInt[idx2]))
                pplist.emplace_back(std::make_pair(idx1, idx2)); // insert to vector
        } else {
            break;
        }
    }
}
inline void sweepEP(const int eidx, const std::pair<int,int> &edge,
        const Interval1 &xinterval, const SPair &yinterval,
        TmpList &pelist, const int n, const VectorXs &x, const std::vector<int> &pIntervalId,
        const std::vector<Interval1> &intervals, const std::vector<SPair> &yInt) {

    int leftIdx, rightIdx;
    if (x(2*edge.first) < x(2*edge.second)) {
        leftIdx = edge.first; rightIdx = edge.second;
    } else {
        leftIdx = edge.second; rightIdx = edge.first;
    }
    int particleIntIdx = pIntervalId[leftIdx];

    int j = particleIntIdx + 1;
    // first particle interval after leftIdx, so leftIdx != vidx
    while (j < n && xinterval.M >= intervals[j].m) {
        int vidx = intervals[j].id;
        if (rightIdx != vidx)
            if (!beNotOverlap(yinterval, yInt[vidx]))
                pelist.emplace_back(std::make_pair(vidx, eidx));
        ++j;
    }
}
inline void sweepAllPE(TmpList &pelist, const int n, const int nEdge,
        const std::vector<std::pair<int,int> > &edges,
        const std::vector<Interval1> &intervals, const std::vector<Interval1> &intervalsEdge,
        const std::vector<SPair> &yInt, const std::vector<SPair> &yIntEdge) {
    int i = 0;
    int j = 0;
    while (i < n) {
        while (j < nEdge && intervalsEdge[j].m < intervals[i].m) ++j;
        if (j >= nEdge) break;
        int k = j;
        int vidx = intervals[i].id;
        const SPair &yinterval = yInt[vidx];
        while (k < nEdge && intervalsEdge[k].m <= intervals[i].M) {
            int eidx = intervalsEdge[k].id;
            if (vidx != edges[eidx].first && vidx != edges[eidx].second)
                if (!beNotOverlap(yIntEdge[eidx], yinterval))
                    pelist.emplace_back(std::make_pair(vidx, eidx));
            ++k;
        }
        ++i;
    }
}

void ContestDetector::findCollidingPairs(const TwoDScene &scene, const VectorXs &x,
        PPList &pppairs, PEList &pepairs, PHList &phpairs)
{
#ifdef DebugContest
    std::clock_t start;
    double duration;
    start = std::clock();
#endif
    std::vector<scalar> r = scene.getRadii(); // a copy of r
    int n = r.size();
    const std::vector<std::pair<int,int> > edges = scene.getEdges();
    const std::vector<scalar>& rEdge = scene.getEdgeRadii();
    int nEdge = rEdge.size();

    // Project edges to X; Modidy r(vidx) if < r(edix)
    std::vector<Interval1> intervalsEdge;
    std::vector<SPair> yIntEdge;
    projectEToX(nEdge, edges, rEdge, intervalsEdge, yIntEdge, x, r);
    sort(intervalsEdge.begin(), intervalsEdge.end());

#ifdef DebugContest
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "Project edges to X and sort: " << duration << std::endl;
    start = std::clock();
#endif
    // Project particles to X
    std::vector<Interval1> intervals;
    std::vector<SPair> yInt;
    projectPToX(n, x, r, intervals, yInt);
    sort(intervals.begin(), intervals.end());
#ifdef DebugContest
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "Project particles to X and sort: " << duration << std::endl;
    start = std::clock();
#endif
#ifdef DebugContest2
    for (const auto &i: intervalsEdge) std::cout << i.m << ',' << i.M << ';'; std::cout << std::endl;
    for (const auto &i: intervals) std::cout << i.m << ',' << i.M << ';'; std::cout << std::endl;
#endif

    /* Sweep and Prune */

    // SaP particle-particle; Get interval index of each particle.
    std::vector<int> pIntervalId(n);
    TmpList tmppplist;
    for (int i = 0; i < n; ++i) {
        pIntervalId[intervals[i].id] = i;
        sweepPP(i, tmppplist, n, intervals, yInt);
    }
    pppairs = PPList(tmppplist.begin(), tmppplist.end()); // overwrite
#ifdef DebugContest
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "#PP:" << pppairs.size() << "; SaP PP: " << duration << std::endl;
    start = std::clock();
#endif

#ifdef DebugContest2
    std::cout << "Idx of particle intervals: ";
    for (const auto &i: pIntervalId) std::cout << i << ','; std::cout << std::endl;
    for (const auto &i: pppairs) std::cout << i.first << ',' << i.second << ';'; std::cout << std::endl;
#endif

    // SaP particle-edge
    TmpList tmppelist;
    for(int i = 0; i < nEdge; ++i) {
        // SaP all particles whose interavl > edge's interval
        int eidx = intervalsEdge[i].id;
        sweepEP(eidx, edges[eidx], intervalsEdge[i], yIntEdge[eidx],
            tmppelist, n, x, pIntervalId, intervals, yInt);
    }
    // SaP all edges whose interavl >= particles's interval
#ifdef DebugContest
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "#EP:" << tmppelist.size() << "; SaP EP: " << duration << std::endl;
    start = std::clock();
#endif
    sweepAllPE(tmppelist, n, nEdge, edges, intervals, intervalsEdge, yInt, yIntEdge);
#ifdef DebugContest
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "SaP PE: " << duration << std::endl;
    start = std::clock();
#endif
    pepairs = PEList(tmppelist.begin(), tmppelist.end());
#ifdef DebugContest
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "#PE:" << pepairs.size() << "; vector -> set: " << duration << std::endl;
    start = std::clock();
#endif
#ifdef DebugContest2
    for (const auto &i: pepairs) std::cout << i.first << ',' << i.second << ';'; std::cout << std::endl;
#endif


    // Add all particle-halfplane pairs
    TmpList tmpphlist;
    int nHalfplane = scene.getNumHalfplanes();
    tmpphlist.reserve(n * nHalfplane);
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < nHalfplane; ++j) {
            tmpphlist.emplace_back(std::make_pair(i, j));
        }
    phpairs = PHList(tmpphlist.begin(), tmpphlist.end());
#ifdef DebugContest
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "SaP PH: " << duration << std::endl;
    start = std::clock();
#endif


#ifdef DebugContest2
    std::cout << std::endl;
    // usleep(500000);
#endif

    // calc grids time complexity first

    // todo: store Y pairs

    // todo: in parallel using a radix sort

    // Spatial Partition
    // - Uniform Grid
    // Grid method
    // good for many simple moving objects of about the same size (e.g., many
    // moving discs with similar radii)

    // – Bounding Volume Hierarchy (BVH) method
    // good for few moving objects with complex
    // geometry



    // todo: in parallel
    // thread at last


}

/*

todo: test every function

,0,1,p0,p1,e0,e1
NULL,0.0662924,0.0216355,
sweepPP,1.563,0.056,100k,10k
+sweepEP,6.499,0.28,301429,2768,822,119138
+yInterval,,0.195
[0]
35.94s user 0.29s system 99% cpu 36.373 total
135.44s user 1.09s system 98% cpu 2:18.94 total

[1]
14.87s user 0.26s system 91% cpu 16.467 total
216.87s user 1.72s system 97% cpu 3:43.35 total
211.80s user 1.43s system 99% cpu 3:34.81 total
*/

/*

Abstract

    To perform massively parallel sweep-and-prune (SaP), we mitigate the great density of intervals along the axis of sweep by using principal component analysis to choose the best sweep direction, together with spatial subdivisions to further reduce the number of false positive overlaps. Our algorithm implemented entirely on GPUs using the CUDA framework can handle a million moving objects at interactive rates. As application of our algorithm, we demonstrate the real-time simulation of very large numbers of particles and rigid-body dynamics.
three broad-phase CD algorithms provided in Bullet, which are BoxPruning, ArraySaP and an AABB dynamic tree. The first two algorithms are based on SaP and the last one using a dynamic bounding volume hierarchy.

1. efficiently utilizes the collision results introduced by static objects, which are cached from the previous time step.

2. geometric intersections among simply-shaped objects such
as axis-aligned bounding boxes (AABBs) or spheres. O(n log^2 n + k) in 3D

3.   sorting-based  technique  known  as  sweep and prune (SaP)
 O(n + s), s is the number of swapping operations required to maintain the sorted order of the objects.

4. Spatial subdivision: Different types of uniform and non-uniform grids such as the k-d tree,  octree and BSP-tree

*/
