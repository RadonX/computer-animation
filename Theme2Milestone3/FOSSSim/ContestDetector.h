#ifndef CONTEST_DETECTOR_H
#define CONTEST_DETECTOR_H

#include "CollisionDetector.h"
#include <vector>
#include <set>

typedef std::set<std::pair<int, int> > PPList;
typedef std::set<std::pair<int, int> > PEList;
typedef std::set<std::pair<int, int> > PHList;
typedef std::vector<std::pair<int, int> > TmpList;
typedef std::pair<scalar, scalar> SPair;
struct Interval1 {
    scalar m, M;
    int id;
    Interval1(scalar m, scalar M, int id):
        m(m), M(M), id(id) {}

    bool operator< (const Interval1 &other) const {
        return m < other.m;
    }
};

class ContestDetector : public CollisionDetector
{
 public:
  ContestDetector() {}

  virtual void performCollisionDetection(const TwoDScene &scene, const VectorXs &qs, const VectorXs &qe, DetectionCallback &dc);

 private:
  void findCollidingPairs(const TwoDScene &scene, const VectorXs &x, PPList &pppairs, PEList &pepairs, PHList &phpairs);

  // static
};

#endif
